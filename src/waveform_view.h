/*
  copyright (C) 2012-2020 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ---------------------------------------------------------------

  MhWaveformView is a Gtk widget based on GtkDrawingArea.
  It is an extension to the simple convenience widget included with
  libwaveform that handles multiple regions.

  One difference is that it supports multiple simultaneous sources.

*/
#ifndef __mh_waveform_view_h__
#define __mh_waveform_view_h__

#include <glib.h>
#include <gtk/gtk.h>
#include "waveform/ui-typedefs.h"
#include "waveform/utils.h"
#include "waveform/waveform.h"
#include "document.h"

G_BEGIN_DECLS

#define TYPE_MH_WAVEFORM_VIEW            (mh_waveform_view_get_type ())
#define MH_WAVEFORM_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MH_WAVEFORM_VIEW, MhWaveformView))
#define MH_WAVEFORM_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MH_WAVEFORM_VIEW, MhWaveformViewClass))
#define IS_MH_WAVEFORM_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MH_WAVEFORM_VIEW))
#define IS_MH_WAVEFORM_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MH_WAVEFORM_VIEW))
#define MH_WAVEFORM_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MH_WAVEFORM_VIEW, MhWaveformViewClass))

typedef struct _MhWaveformView        MhWaveformView;
typedef struct _MhWaveformViewClass   MhWaveformViewClass;
typedef struct _MhWaveformViewPrivate MhWaveformViewPrivate;

struct _MhWaveformView {
	GtkDrawingArea         parent_instance;
	Document*              doc;
	float                  zoom;
	MhWaveformViewPrivate* priv;
};

typedef union {
	gint64 frames;
} Position;

#ifdef __mh_waveform_view_private__
typedef struct {
	DataPart*       data;
	WaveformActor*  actor;
	gint64          source_to_view;  // frames. eg if a region is cut, this will be negative.
	Position        canvas_position; // frames
	gboolean        show;            // false if outside viewport
} MhWaveformViewPart;

#endif

struct _MhWaveformViewClass {
	GtkDrawingAreaClass parent_class;
};


GType           mh_waveform_view_get_type      () G_GNUC_CONST;

MhWaveformView* mh_waveform_view_new           ();
void            mh_waveform_view_load_file     (MhWaveformView*, const char*); // be careful, it force loads, even if already loaded.
void            mh_waveform_view_set_document  (MhWaveformView*, Document*);
void            mh_waveform_view_set_tools     (MhWaveformView*, Mainwindow*, ToolbarItem[], int);
void            mh_waveform_view_set_sensitive (MhWaveformView*, gboolean);
void            mh_waveform_view_set_zoom      (MhWaveformView*, float);
void            mh_waveform_view_set_start     (MhWaveformView*, int64_t);
void            mh_waveform_view_set_colour    (MhWaveformView*, uint32_t fg, uint32_t bg);
void            mh_waveform_view_set_show_rms  (MhWaveformView*, gboolean);
void            mh_waveform_view_set_vscale    (MhWaveformView*, gfloat);

WaveformContext* mh_waveform_view_get_canvas    (MhWaveformView*);


G_END_DECLS

#endif
