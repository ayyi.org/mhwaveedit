/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  waveform_view.c
 |
 |  MhWaveformView is a Gtk widget based on GtkDrawingArea.
 |  It displays an audio waveform represented by a Waveform object.
 |
 */

#define __mh_waveform_view_private__
#include "config.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "agl/gtk.h"
#include "waveform/waveform.h"
#include "waveform/context.h"
#include "waveform/actor.h"
#include "waveform/actors/labels.h"
#include "waveform/actors/grid.h"
#include "waveform/actors/ruler.h"
#include "debug.h"
#include "icon.h"
#include "mainloop.h"
#include "mainwindow.h" // for ToolbarItem
#include "wavepool.h"
#include "waveform_view.h"

typedef enum {
	CHANGE_DIM = 1
} ChangeType;

struct _MhWaveformViewPrivate {
	AMPromise*      ready;
	gboolean        gl_init_done;
	AGlActor*       root;
	WaveformContext* context;
	AGlActor*       toolbar;
	AGlActor*       grid;
	AGlActor*       ruler;
	AGlActor*       labels;
	GList*          parts;           // type MhWaveformViewPart
	off_t           old_viewstart,old_viewend; // copy of Document properties. Used to detect changes.
	ChangeType      change;
	gulong          update;          // GSourceID
};

#define TRIM_REGION_TO_VIEWPORT // this is the preferred way and is working better, so the other path can be removed fairly soon.
                                // the downside to this is that it makes animating more difficult

#define DIRECT 1

static GdkGLConfig*   glconfig = NULL;

static gboolean       dragmode = FALSE;
static off_t          dragstart;
static off_t          dragend;
static gboolean       autoscroll = false;
static gpointer       autoscroll_source = NULL;
static MhWaveformView*autoscroll_view;
static gfloat         autoscroll_amount;
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
static GTimeVal       autoscroll_start;
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

static ToolbarItem*   toolbar_items;
static int            toolbar_size;
static Mainwindow*    main_window; // FIXME

#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

static void     draw                                  (MhWaveformView*);
static int      mh_waveform_view_get_width            (MhWaveformView*);
static off_t    calc_sample                           (MhWaveformView*, gfloat x, gfloat xmax);
static gint     calc_x                                (MhWaveformView*, off_t sample, off_t width);
static gint     calc_x_unbounded                      (MhWaveformView*, off_t sample, off_t width);
//static gint     calc_x_source                         (MhWaveformView*, off_t sample, off_t width);
static gint     calc_x_source_unbound                 (MhWaveformView*, off_t sample, off_t width);
static gint     calc_x_for_part                       (MhWaveformView*, MhWaveformViewPart*, off_t sample, off_t width);
static gint     calc_x_relative                       (MhWaveformView*, off_t sample, off_t width);

static AGl* agl = NULL;
enum {
  TEXTURE_BG = 0, // TODO not yet used
  TEXTURE_ICON_OPEN,
  TEXTURE_ICON_OPEN_ON,
  TEXTURE_ICON_SAVE,
  TEXTURE_ICON_SAVE_ON,
  TEXTURE_ICON_UNDO,
  TEXTURE_ICON_UNDO_ON,
  TEXTURE_ICON_REDO,
  TEXTURE_ICON_REDO_ON,
  TEXTURE_ICON_CUT,
  TEXTURE_ICON_CUT_ON,
  TEXTURE_ICON_PASTE,
  TEXTURE_ICON_PASTE_ON,
  TEXTURE_ICON_PASTEOVER,
  TEXTURE_ICON_PASTEOVER_ON,
  TEXTURE_ICON_DELETE,
  TEXTURE_ICON_DELETE_ON,
  TEXTURE_ICON_CURSORSTART,
  TEXTURE_ICON_CURSORSTART_ON,
  TEXTURE_ICON_CURSOREND,
  TEXTURE_ICON_CURSOREND_ON,
  TEXTURE_ICON_PLAY,
  TEXTURE_ICON_PLAY_ON,
  TEXTURE_ICON_PLAY_SELECTION,
  TEXTURE_ICON_PLAY_SELECTION_ON,
  TEXTURE_ICON_STOP,
  TEXTURE_ICON_STOP_ON,
  TEXTURE_ICON_LOOP,
  TEXTURE_ICON_LOOP_ON,
  TEXTURE_ICON_FOLLOW,
  TEXTURE_ICON_FOLLOW_ON,
  TEXTURE_ICON_BOUNCE,
  TEXTURE_ICON_BOUNCE_ON,
  TEXTURE_ICON_RECORD,
  TEXTURE_ICON_RECORD_ON,
  TEXTURE_ICON_MIXER,
  TEXTURE_ICON_MIXER_ON,
  TEXTURE_MAX,
};
static GLuint bg_textures[TEXTURE_MAX + 1/* TODO */];

#define HAS_ALPHA_FALSE 0
#define HAS_ALPHA_TRUE 1
#define BITS_PER_PIXEL_8 8

#include "button_open.xpm"
#include "button_save.xpm"
#include "button_undo.xpm"
#include "button_redo.xpm"
#include "button_cut.xpm"
#include "button_paste.xpm"
#include "button_pasteover.xpm"
#include "button_delete.xpm"
#include "button_cursorstart.xpm"
#include "button_cursorend.xpm"
#include "button_play.xpm"
#include "button_playselection.xpm"
#include "button_stop.xpm"
#include "button_loop.xpm"
#include "button_follow.xpm"
#include "button_bounce.xpm"
#include "button_record.xpm"
#include "button_mixer.xpm"

extern WF* wf;
enum {
    PROMISE_DISP_READY,
    //PROMISE_WAVE_READY,
    PROMISE_MAX
};
#define promise(A) ((AMPromise*)g_list_nth_data(view->priv->ready->children, A))

G_DEFINE_TYPE_WITH_PRIVATE (MhWaveformView, mh_waveform_view, GTK_TYPE_DRAWING_AREA)
enum  {
	MH_WAVEFORM_VIEW_DUMMY_PROPERTY
};

static gboolean mh_waveform_view_configure_event      (GtkWidget*, GdkEventConfigure*);
static gboolean mh_waveform_view_on_expose            (GtkWidget*, GdkEventExpose*);
static gboolean mh_waveform_view_motion_notify        (GtkWidget*, GdkEventMotion*);
static gboolean mh_waveform_view_leave_notify         (GtkWidget*, GdkEventCrossing*);
static gboolean mh_waveform_view_button_release       (GtkWidget*, GdkEventButton*);
static void     mh_waveform_view_realize              (GtkWidget*);
static void     mh_waveform_view_unrealize            (GtkWidget*);
static void     mh_waveform_view_destroy              (GtkObject*);
static void     mh_waveform_view_allocate             (GtkWidget*, GdkRectangle*);
static void     mh_waveform_view_finalize             (GObject*);
//static gint     mh_waveform_view_event                (GtkWidget*, GdkEvent*, gpointer _arrange);
static gboolean mh_waveform_view_button_press         (GtkWidget*, GdkEventButton*);
static void     mh_waveform_view_init_display         (MhWaveformView*);
static void     mh_waveform_view_on_change            (MhWaveformView*, ChangeType);
static void     mh_waveform_view_on_document_change   (Document*, gpointer);
static void     mh_waveform_view_on_selection_change  (Document*, gpointer);
static void     mh_waveform_view_on_cursor_change     (Document*, gboolean rolling, gpointer);
static void     mh_waveform_view_redraw_range         (MhWaveformView*, off_t, off_t);
static void     mh_waveform_view_update_part_list     (MhWaveformView*);
static void    _mh_waveform_view_set_actors           (MhWaveformView*);
static void    _mh_waveform_view_set_actor            (MhWaveformView*, MhWaveformViewPart*);
static void     mh_waveform_view_set_region           (MhWaveformView*, MhWaveformViewPart*);
static WaveformActor* mh_waveform_add_actor           (MhWaveformView*, MhWaveformViewPart*);
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
static int      chunk_view_autoscroll                 (gpointer timesource, GTimeVal*, gpointer);
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
static AGlActor* extras_actor                         (WaveformActor*);

#include "gl_button.c"


static bool
__init ()
{
	dbg(2, "...");

	if(!glconfig){
		gtk_gl_init(NULL, NULL);

		glconfig = gdk_gl_config_new_by_mode( GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE );
		if (!glconfig) { gerr ("Cannot initialise gtkglext."); return false; }
	}

	return true;
}


static MhWaveformView*
construct ()
{
	MhWaveformView* self = (MhWaveformView*) g_object_new (TYPE_MH_WAVEFORM_VIEW, NULL);
	MhWaveformViewPrivate* priv = self->priv;

	gtk_widget_add_events ((GtkWidget*) self, (gint) ((GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK) | GDK_POINTER_MOTION_MASK));
	if(!gtk_widget_set_gl_capability((GtkWidget*)self, glconfig, agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE)){
		pwarn("failed to set gl capability");
	}

	priv->ready = am_promise_new(self);
	am_promise_when(priv->ready, am_promise_new(self), am_promise_new(self), NULL);

#if 0
	void _on_ready(gpointer _view, gpointer _c)
	{
		mh_waveform_view_allocate (_view, &((GtkWidget*)_view)->allocation);
	}
	am_promise_add_callback(priv->ready, _on_ready, NULL);
#endif

	return self;
}


MhWaveformView*
mh_waveform_view_new ()
{
	PF;
	int width = 256, height = 64;

	g_return_val_if_fail(/*glconfig || */__init(), NULL);

	MhWaveformView* view = construct ();
	MhWaveformViewPrivate* v = view->priv;
	GtkWidget* widget = (GtkWidget*)view;

#ifdef HAVE_GTK_2_18
	gtk_widget_set_can_focus(widget, TRUE);
#endif
	gtk_widget_set_size_request(widget, width, height);

	//g_signal_connect((gpointer)widget, "event", G_CALLBACK(mh_waveform_view_event), NULL);

#if 0 // hopefully is no longer needed
	gboolean mh_waveform_view_load_new_on_idle(gpointer _view)
	{
		MhWaveformView* view = _view;
		g_return_val_if_fail(view, G_SOURCE_REMOVE);

#if 0 // impossible to get here?
		MhWaveformViewPrivate* v = view->priv;
		if(!v->root){
			v->root = (AGlActor*)agl_actor__new_root((GtkWidget*)view);
			if(v->root){
				v->root->region = (AGliRegion){0, 0, 100, 20};
			}
		}
#endif

		if(!promise(PROMISE_DISP_READY)->is_resolved){
			mh_waveform_view_init_display(view);
			gtk_widget_queue_draw((GtkWidget*)view); //testing.
		}

		return G_SOURCE_REMOVE;
	}
	g_idle_add(mh_waveform_view_load_new_on_idle, view);
#endif

	void display_ready(AGlActor* a)
	{
		MhWaveformView* view = (MhWaveformView*)((AGlScene*)a)->gl.gdk.widget;
		MhWaveformViewPrivate* v = view->priv;
		v->context = wf_context_new(v->root);

		mh_waveform_view_init_display(view);

		am_promise_resolve(promise(PROMISE_DISP_READY), NULL);
	}

	v->root = agl_actor__new_root(widget);
	v->root->init = display_ready;
	((AGlScene*)v->root)->user_data = view;
	((AGlScene*)v->root)->enable_animations = false;

	agl_actor__add_child(v->root, extras_actor(NULL));

	return view;
}


static void
_mh_waveform_view_allocate_actor (MhWaveformView* view, MhWaveformViewPart* part)
{
	WaveformActor* actor = part->actor;
	MhWaveformViewPrivate* v = view->priv;
	if(!actor) return;

	int window_width = mh_waveform_view_get_width(view);
	guint64 end = part->data->position + part->data->length;
#ifdef TRIM_REGION_TO_VIEWPORT
	float x      = MAX(0,            calc_x_for_part(view, part, 0, window_width));
	float len_px = MIN(window_width, calc_x_for_part(view, part, end, window_width)) - x;
	len_px = MAX(len_px, 1.0); // temporary until we find a way to not draw invisible parts.

	//float len_px = calc_x_relative(view, part->data->length, window_width);
#else
	float x = calc_x_source_unbound(view, part->data->position, window_width);
	float len_px = calc_x_unbounded(view, end, window_width) - x;
#endif
	if(_debug_ > -1){
		int i = g_list_index(v->parts, part);
		dbg(1, "%i: frames: %04"PRIu64" -> len=%04Li (%.1f -> %.1f px)", i, part->data->position, part->data->length, x, x + len_px);
	}
	g_return_if_fail(len_px > 0.0);

	#define BOTTOM_BORDER 10

	// we can choose here whether or not to animate
#if 0
	wf_actor_set_rect(actor, &(WfRectangle){
		x,
		BUTTON_SIZE,
		len_px,
		((GtkWidget*)view)->allocation.height - BUTTON_SIZE - BOTTOM_BORDER
	});
#else
	AGlActor* a = (AGlActor*)actor;

	a->region = (AGlfRegion){
		x,
		BUTTON_SIZE,
		x + len_px,
		((GtkWidget*)view)->allocation.height - BOTTOM_BORDER
	};
	agl_actor__set_size(a);
#endif
}


static void
_mh_waveform_view_set_actor (MhWaveformView* view, MhWaveformViewPart* part)
{
	// part could be outside viewport.
	// -currently actors cannot be disabled, so we move it offscreen if we dont want it to be shown

	g_return_if_fail(part && part->actor);

	Document* d = view->doc;
	MhWaveformViewPrivate* v = view->priv;
	WaveformActor* actor = part->actor;

	if(!part->show){
		wf_actor_set_rect(actor, &(WfRectangle){-100, 0, 1, 1});
		return;
	}

	/*
	{
		//check zoom:
		//float check = (x + len) * ((float)width) / ((float))
		Document* d = view->doc;
		float px_gain = len_px / ((float)window_width);
		float fr_gain = ((float)part->data->length) / (((float)d->viewend) - ((float)d->viewstart));
		dbg(0, "gain=%.4f=%.4f z=%.4f", px_gain, fr_gain, (((float)d->viewend) - ((float)d->viewstart)) / ((float)window_width));
	}
	*/
	if(v->ready->is_resolved){
		_mh_waveform_view_allocate_actor (view, part);
	}
}


static void
_mh_waveform_view_set_actors (MhWaveformView* view)
{
	MhWaveformViewPrivate* v = view->priv;

	// is not clear what should happen here and what in view_change

	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		if(part->actor){
			_mh_waveform_view_set_actor(view, part);
		}
	}
}


/*
 *  This must only be called from set_document
 *
 *	If filename is NULL the display will be cleared.
 */
void
mh_waveform_view_load_file (MhWaveformView* view, const char* filename)
{
	MhWaveformViewPrivate* v = view->priv;

#if 1
	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		WaveformActor* actor = part->actor;

		agl_actor__remove_child(v->root, (AGlActor*)actor);
	}
	g_list_free0(v->parts);
#else

	if(v->actor){
		wf_canvas_remove_actor(v->context, v->actor);
		v->actor = NULL;
	}
	else dbg(2, " ... no actor");
#endif

	if(!filename){
		gtk_widget_queue_draw((GtkWidget*)view);
		return;
	}

	typedef struct
	{
		MhWaveformView* view;
	} C;

	C* c = AGL_NEW(C,
		.view = view,
	);

#if 1
	mh_waveform_view_update_part_list(view);
#else
	WaveformActor* actor = v->actor = wf_canvas_add_new_actor(v->context, view->waveform);
	wf_actor_set_region(actor, &(WfSampleRegion){0, waveform_get_n_frames(view->waveform)});
#endif

	void _on_display_ready (gpointer _view, gpointer _c)
	{
		C* c = _c;
		MhWaveformView* view = c->view;
		MhWaveformViewPrivate* v = view->priv;

		mh_waveform_view_update_part_list(view);

		_mh_waveform_view_set_actors(c->view);

		gtk_widget_queue_draw((GtkWidget*)c->view);

		g_free(c);
	}
	am_promise_add_callback(promise(PROMISE_DISP_READY), _on_display_ready, c);

#if 0
	void waveform_finalize_notify (gpointer _view, GObject* was)
	{
		dbg(0, "!");
		MhWaveformView* view = _view;
		view->waveform = NULL;
	}
	g_object_weak_ref((GObject*)view->waveform, waveform_finalize_notify, view);
#endif
}


void
mh_waveform_view_set_document (MhWaveformView* view, Document* doc)
{
	PF;

	if (view->doc == doc) return;

	if (view->doc) {
		g_signal_handlers_disconnect_matched(GTK_OBJECT(view->doc), G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, view);
		g_object_unref(GTK_OBJECT(view->doc));
	}
	if (view->doc = doc) { 
		gtk_object_ref(GTK_OBJECT(doc)); 
		gtk_object_sink(GTK_OBJECT(doc)); 
		g_signal_connect(GTK_OBJECT(doc), "view_changed",      GTK_SIGNAL_FUNC(mh_waveform_view_on_document_change), view);
		g_signal_connect(GTK_OBJECT(doc), "state_changed",     GTK_SIGNAL_FUNC(mh_waveform_view_on_document_change), view);
		g_signal_connect(GTK_OBJECT(doc), "selection_changed", GTK_SIGNAL_FUNC(mh_waveform_view_on_selection_change), view);
		g_signal_connect(GTK_OBJECT(doc), "cursor_changed",    GTK_SIGNAL_FUNC(mh_waveform_view_on_cursor_change), view);
	}

	mh_waveform_view_load_file(view, doc->filename);

	mh_waveform_view_on_document_change(view->doc, view);
}


void
mh_waveform_view_add_buttons (MhWaveformView* view)
{
	MhWaveformViewPrivate* v = view->priv;

	void track_arm_toggle(AGlActor* actor, gpointer _) {
		dbg(0, "button press");
	}

	void on_info(AGlActor* actor, char* message)
	{
		dbg(0, "%s", message);
		status_bar_set_rollover(main_window->statusbar, (char*)message);
	}

	v->toolbar = agl_actor__add_child(v->root, agl_actor__new(AGlActor));
	v->toolbar->name = "Toolbar";

	int x = 0;
	guint icon = TEXTURE_ICON_OPEN;
	int i; for(i=0;i<toolbar_size;i++,x+=BUTTON_SIZE+4){
		ToolbarItem* item = &toolbar_items[i];
		dbg(1, "  %i %s", i, item->text);
		if(item->text){
			AGlActor* button = agl_actor__add_child(v->toolbar, gl_button(icon, (ButtonAction)item->callback, NULL, main_window));
			button->disabled = !!item->list2;
			button->region = (AGlfRegion){x, 1, x + BUTTON_SIZE, 1 + BUTTON_SIZE};
			button->name = g_strdup_printf(item->text, 16);
			((ButtonActor*)button)->tooltip = on_info;

			if(item->list2){
				GList* l = g_list_append(*item->list2, button);
				*item->list2 = l;
			}

			icon += 2;
		}
	}
}


void
mh_waveform_view_set_tools (MhWaveformView* view, Mainwindow* window, ToolbarItem items[], int n)
{
	toolbar_items = items;
	toolbar_size = n;
	main_window = window;

	//mh_waveform_view_add_buttons (view);
}


void
mh_waveform_view_set_sensitive (MhWaveformView* view, gboolean sensitive)
{
	MhWaveformViewPrivate* v = view->priv;

	if(v->toolbar){
		v->toolbar->disabled = !sensitive;
		gtk_widget_queue_draw((GtkWidget*)view);
	}
}


void
mh_waveform_view_set_zoom (MhWaveformView* view, float zoom)
{
	MhWaveformViewPrivate* _view = view->priv;
	#define MAX_ZOOM 51200.0 //TODO
	g_return_if_fail(view);
	dbg(0, "zoom=%.2f", zoom);
	view->zoom = CLAMP(zoom, 1.0, MAX_ZOOM);

#if 1
	GList* l = _view->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		WaveformActor* actor = part->actor;

		g_return_if_fail(part->data);
#ifdef TRIM_REGION_TO_VIEWPORT
		mh_waveform_view_set_region(view, part);
		/*
		Document* d = view->doc;
		off_t start = MAX(part->data->position, d->viewstart);
		off_t end = MIN(part->data->position + part->data->length, d->viewend);
		int len = end - start;
		WfSampleRegion region = {
			start,
			len
		};
		wf_actor_set_region(actor, &region);
		*/
#else
		//TODO
		//WfSampleRegion region = {view->start_frame, (waveform_get_n_frames(view->waveform) - view->start_frame) / view->zoom};
		off_t start = part->data->position;
		int len = part->data->length;
		off_t start_cropped = MAX(start, view->start_frame);
		int len_cropped = len;

		WfSampleRegion region = {
			//MAX(view->start_frame, part->data->position),
			start_cropped,
			//(waveform_get_n_frames(view->waveform) - view->start_frame) / view->zoom
			len_cropped
		};
		dbg(0, "  %Li - %i", start_cropped, len);
		wf_actor_set_region(actor, &region);
#endif
	}

	if(!((AGlActor*)((MhWaveformViewPart*)_view->parts->data)->actor)->root->draw) gtk_widget_queue_draw((GtkWidget*)view);
#else
	WfSampleRegion region = {view->start_frame, (waveform_get_n_frames(view->waveform) - view->start_frame) / view->zoom};
	wf_actor_set_region(view->priv->actor, &region);

	if(!_view->actor->canvas->draw) gtk_widget_queue_draw((GtkWidget*)view);
#endif
}


#if 0
void
mh_waveform_view_set_start (MhWaveformView* view, int64_t start_frame)
{
	MhWaveformViewPrivate* _view = view->priv;

#warning mh_waveform_view_set_start: TODO length setting is not correct for cuts
	uint32_t len_fr = waveform_get_n_frames(view->waveform);
	view->start_frame = CLAMP(start_frame, 0, (int64_t)waveform_get_n_frames(view->waveform) - 10);
	if(view->start_frame > waveform_get_n_frames(view->waveform) - len_fr) pwarn("suspicious start_frame");
	view->start_frame = MIN(view->start_frame, waveform_get_n_frames(view->waveform) - len_fr);
	dbg(0, "start=%Lu", view->start_frame);
#if 1
	GList* l = _view->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		WaveformActor* actor = part->actor;

		wf_actor_set_region(actor, &(WfSampleRegion){
			view->start_frame,
			len_fr
		});
	}
	if(!((MhWaveformViewPart*)_view->parts->data)->actor->canvas->draw) gtk_widget_queue_draw((GtkWidget*)view);
#else
	wf_actor_set_region(view->priv->actor, &(WfSampleRegion){
		view->start_frame,
		len_fr
	});
	if(!_view->actor->canvas->draw) gtk_widget_queue_draw((GtkWidget*)view);
#endif
}
#endif


static void
mh_waveform_view_set_region (MhWaveformView* view, MhWaveformViewPart* part)
{
	Document* d = view->doc;
	MhWaveformViewPrivate* v = view->priv;
	AGlScene* scene = (AGlScene*)v->root;

#ifdef TRIM_REGION_TO_VIEWPORT

	if(!waveform_get_n_frames(part->actor->waveform)){
		return;
	}
	g_return_if_fail(part->source_to_view <= d->viewstart);

	gint64 viewstart_translated_to_source_coordinates = d->viewstart - (part->canvas_position.frames - part->data->position);
	gint64 viewend_translated_to_source_coordinates = d->viewend - (part->canvas_position.frames - part->data->position);

	WfSampleRegion region = {
		.start = MAX(((gint64)part->data->position), viewstart_translated_to_source_coordinates) // crops the region to the start of the viewport
	};
	guint64 end = MIN(part->data->position + part->data->length - 1, viewend_translated_to_source_coordinates);
	region.len = end - region.start;

	wf_actor_set_region(part->actor, &region);

#else
					//TODO repair following refactor
					guint64 start = MAX(d->viewstart, part->data->position);
					guint64 end = MIN(d->viewend - part->source_to_view, part->data->position + part->data->length);
	wf_actor_set_region(part->actor, &(WfSampleRegion){part->data->position, part->data->length});
#endif
}


void
mh_waveform_view_set_colour (MhWaveformView* view, uint32_t fg, uint32_t bg)
{
	MhWaveformViewPrivate* _view = view->priv;

#if 1
	GList* l = _view->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		WaveformActor* actor = part->actor;

		if(actor) wf_actor_set_colour(actor, fg);
	}
#else
	wf_actor_set_colour(view->priv->actor, fg, bg);
#endif
}


void
mh_waveform_view_set_show_rms (MhWaveformView* view, gboolean _show)
{
	//FIXME this idle hack is because wa is not created until realise.

	static gboolean show; show = _show;

	gboolean _on_idle(gpointer _view)
	{
		MhWaveformView* view = _view;

		view->priv->context->show_rms = show;
		gtk_widget_queue_draw((GtkWidget*)view);

		return G_SOURCE_REMOVE;
	}
	g_idle_add(_on_idle, view);
}


static void
mh_waveform_view_realize (GtkWidget* base)
{
	PF2;
	MhWaveformView* view = (MhWaveformView*)base;
	GTK_WIDGET_SET_FLAGS (base, GTK_REALIZED);

	GdkWindowAttr attrs = {
		.window_type = GDK_WINDOW_CHILD,
		.width = base->allocation.width,
		.wclass = GDK_INPUT_OUTPUT,
		.event_mask = gtk_widget_get_events(base) | GDK_EXPOSURE_MASK | GDK_KEY_PRESS_MASK
	};

	_g_object_unref0(base->window);
	base->window = gdk_window_new (gtk_widget_get_parent_window(base), &attrs, 0);
	gdk_window_set_user_data (base->window, view);
	gtk_widget_set_style (base, gtk_style_attach(gtk_widget_get_style(base), base->window));
	gtk_style_set_background (gtk_widget_get_style (base), base->window, GTK_STATE_NORMAL);
	gdk_window_move_resize (base->window, base->allocation.x, base->allocation.y, base->allocation.width, base->allocation.height);

	// note that we do not initialize other display aspects yet. Must wait until scene is ready.
}


static void
mh_waveform_view_unrealize (GtkWidget* widget)
{
	PF;
	MhWaveformView* self = (MhWaveformView*)widget;
	MhWaveformViewPrivate* v = self->priv;
	gdk_window_set_user_data (widget->window, NULL);

	if(v->context){
		GList* l = v->parts;
		for(;l;l=l->next){
			MhWaveformViewPart* part = l->data;
			if(part->actor) agl_actor__remove_child(v->root, (AGlActor*)part->actor);
			part->actor = NULL;
		}
		wf_context_free0(v->context);
	}
}


static gboolean
mh_waveform_view_configure_event (GtkWidget* widget, GdkEventConfigure* event)
{
#if 0
	MhWaveformView* self = (MhWaveformView*) widget;
	GtkWidget* widget = (GtkWidget*) ba
	g_return_val_if_fail (event != NULL, FALSE);

	GdkGLContext* glcontext = gtk_widget_get_gl_context(widget);
	GdkGLDrawable* gldrawable = _g_object_ref0 (gtk_widget_get_gl_drawable ((GtkWidget*) self));

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
		return FALSE;
	}

	glViewport ((GLint) 0, (GLint) 0, (GLsizei)widget->allocation.width, (GLsizei)widget->allocation.height);

	gdk_gl_drawable_gl_end (gldrawable);
#endif
	return TRUE;
}


static gboolean
mh_waveform_view_on_expose (GtkWidget* widget, GdkEventExpose* event)
{
	return agl_actor__on_expose(widget, event, ((MhWaveformView*)widget)->priv->root);
}


static void
mh_waveform_view_gl_init (MhWaveformView* view)
{
	PF;
	MhWaveformViewPrivate* v = view->priv;
	AGlScene* sc = (AGlScene*)v->root;

#ifdef USE_SYSTEM_GTKGLEXT
	gdk_gl_drawable_make_current (a->gl.gdk.drawable, a->gl.gdk.context);
#else
    g_assert(G_OBJECT_TYPE(sc->gl.gdk.drawable) == GDK_TYPE_GL_WINDOW);
    gdk_gl_window_make_context_current (sc->gl.gdk.drawable, sc->gl.gdk.context);
#endif
	{
		void create_background ()
		{
			// create an alpha-map gradient texture for use as background

			glEnable(GL_TEXTURE_2D);

			int width = 256;
			int height = 256;
			char* pbuf = g_new0(char, width * height);
			int y; for(y=0;y<height;y++){
				int x; for(x=0;x<width;x++){
					*(pbuf + y * width + x) = ((x+y) * 0xff) / (width * 2);
				}
			}

			dbg(0, "bg_texture=%i", bg_textures[0]);

			int pixel_format = GL_ALPHA;
			glBindTexture  (GL_TEXTURE_2D, bg_textures[0]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, pixel_format, GL_UNSIGNED_BYTE, pbuf);
			if(glGetError() != GL_NO_ERROR) pwarn("gl error binding bg texture!");

			g_free(pbuf);
		}

		void create_icon_svg (const char* name, int icon)
		{
			GdkPixbuf* pixbuf = get_icon(name, 16);
			if(pixbuf){
				dbg(2, "icon: pixbuf=%ix%i %ibytes/px", gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), gdk_pixbuf_get_n_channels(pixbuf));
				glBindTexture   (GL_TEXTURE_2D, bg_textures[icon]);
				glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexImage2D    (GL_TEXTURE_2D, 0, GL_RGBA, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), 0, GL_RGBA, GL_UNSIGNED_BYTE, gdk_pixbuf_get_pixels(pixbuf));
				gl_warn("texture bind");
				g_object_unref(pixbuf);
			}
		}

		void create_icon (GtkWidget* w, const char* name, int icon, char* xpm[])
		{
			int width = 20;
			int height = 20;

			GdkPixbuf* pixbuf = gdk_pixbuf_new_from_xpm_data((const char**)xpm);
			if(pixbuf){
				dbg(2, "icon: pixbuf=%ix%i %ibytes/px", gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), gdk_pixbuf_get_n_channels(pixbuf));
				glBindTexture   (GL_TEXTURE_2D, bg_textures[icon]);
				glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexImage2D    (GL_TEXTURE_2D, 0, GL_RGBA, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), 0, GL_RGBA, GL_UNSIGNED_BYTE, gdk_pixbuf_get_pixels(pixbuf));
				gl_warn("texture bind");
				g_object_unref(pixbuf);
			}
		}

		icons_init();
		glGenTextures(TEXTURE_MAX, bg_textures);
		create_background();
		create_icon((GtkWidget*)view, "open", TEXTURE_ICON_OPEN, button_open_xpm);
		create_icon((GtkWidget*)view, "open-on", TEXTURE_ICON_OPEN_ON, button_open_xpm);
		create_icon((GtkWidget*)view, "save", TEXTURE_ICON_SAVE, button_save_xpm);
		create_icon((GtkWidget*)view, "save-on", TEXTURE_ICON_SAVE_ON, button_save_xpm);
		create_icon((GtkWidget*)view, "undo", TEXTURE_ICON_UNDO, button_undo_xpm);
		create_icon((GtkWidget*)view, "undo-on", TEXTURE_ICON_UNDO_ON, button_undo_xpm);
		create_icon((GtkWidget*)view, "redo", TEXTURE_ICON_REDO, button_redo_xpm);
		create_icon((GtkWidget*)view, "redo-on", TEXTURE_ICON_REDO_ON, button_redo_xpm);
		create_icon((GtkWidget*)view, "cut", TEXTURE_ICON_CUT, button_cut_xpm);
		create_icon((GtkWidget*)view, "paste", TEXTURE_ICON_PASTE, button_paste_xpm);
		create_icon((GtkWidget*)view, "pasteover", TEXTURE_ICON_PASTEOVER, button_pasteover_xpm);
		create_icon((GtkWidget*)view, "delete", TEXTURE_ICON_DELETE, button_delete_xpm);
		create_icon((GtkWidget*)view, "cursorstart", TEXTURE_ICON_CURSORSTART, button_cursorstart_xpm);
		create_icon((GtkWidget*)view, "cursorend", TEXTURE_ICON_CURSOREND, button_cursorend_xpm);
		create_icon((GtkWidget*)view, "play", TEXTURE_ICON_PLAY, button_play_xpm);
		create_icon((GtkWidget*)view, "playselection", TEXTURE_ICON_PLAY_SELECTION, button_playselection_xpm);
		create_icon((GtkWidget*)view, "stop", TEXTURE_ICON_STOP, button_stop_xpm);
		create_icon((GtkWidget*)view, "loop", TEXTURE_ICON_LOOP, button_loop_xpm);
		create_icon((GtkWidget*)view, "follow", TEXTURE_ICON_FOLLOW, button_follow_xpm);
		create_icon((GtkWidget*)view, "bounce", TEXTURE_ICON_BOUNCE, button_bounce_xpm);
		create_icon((GtkWidget*)view, "record", TEXTURE_ICON_RECORD, button_record_xpm);
		create_icon((GtkWidget*)view, "mixer", TEXTURE_ICON_MIXER, button_mixer_xpm);
	}

#ifdef WF_USE_TEXTURE_CACHE
	texture_cache_gen();
#endif
}


static void
mh_waveform_view_init_display (MhWaveformView* view)
{

	g_assert(!promise(PROMISE_DISP_READY)->is_resolved); // init_display is called once only. promise is resolved imediately afterwards

	GtkWidget* widget = (GtkWidget*)view;
	MhWaveformViewPrivate* v = view->priv;

	if(!GTK_WIDGET_REALIZED(widget)) return;

	g_assert(v->root);

	mh_waveform_view_gl_init(view);
	dbg(1, "updating part list...");
	if(view->doc) mh_waveform_view_update_part_list(view);

	mh_waveform_view_add_buttons (view);

	mh_waveform_view_allocate (widget, &widget->allocation);

	dbg(1, "DISP_READY");
	am_promise_resolve(g_list_nth_data(view->priv->ready->children, PROMISE_DISP_READY), NULL);
}


static void
mh_waveform_view_allocate (GtkWidget* widget, GdkRectangle* allocation)
{
	PF;
	g_return_if_fail (allocation);

	MhWaveformView* view = (MhWaveformView*)widget;
	MhWaveformViewPrivate* v = view->priv;

	widget->allocation = (GtkAllocation)(*allocation);
	if ((GTK_WIDGET_FLAGS (widget) & GTK_REALIZED) == 0) return;
	gdk_window_move_resize(widget->window, widget->allocation.x, widget->allocation.y, widget->allocation.width, widget->allocation.height);

	if(v->root->region.x2 == allocation->width && v->root->region.y2 == allocation->height) return;

	if(!promise(PROMISE_DISP_READY)->is_resolved) return;

	mh_waveform_view_on_change(view, CHANGE_DIM);
}


static void
mh_waveform_view_class_init (MhWaveformViewClass * klass)
{
	mh_waveform_view_parent_class = g_type_class_peek_parent (klass);

	GTK_WIDGET_CLASS (klass)->configure_event = mh_waveform_view_configure_event;
	GTK_WIDGET_CLASS (klass)->expose_event = mh_waveform_view_on_expose;
	GTK_WIDGET_CLASS (klass)->realize = mh_waveform_view_realize;
	GTK_WIDGET_CLASS (klass)->unrealize = mh_waveform_view_unrealize;
	GTK_WIDGET_CLASS (klass)->size_allocate = mh_waveform_view_allocate;
	GTK_WIDGET_CLASS (klass)->button_press_event = mh_waveform_view_button_press;
	GTK_WIDGET_CLASS (klass)->motion_notify_event = mh_waveform_view_motion_notify;
	GTK_WIDGET_CLASS (klass)->leave_notify_event = mh_waveform_view_leave_notify;
	GTK_WIDGET_CLASS (klass)->button_release_event = mh_waveform_view_button_release;
	GTK_OBJECT_CLASS (klass)->destroy = mh_waveform_view_destroy;
	G_OBJECT_CLASS (klass)->finalize = mh_waveform_view_finalize;

	agl = agl_get_instance();

	wavepool = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, g_free);
}


static void
mh_waveform_view_init (MhWaveformView * self)
{
	self->zoom = 1.0;
	self->priv = mh_waveform_view_get_instance_private(self);
	self->priv->context = NULL;
	self->priv->parts = NULL;
}


static void
mh_waveform_view_finalize (GObject* obj)
{
	PF;

	MhWaveformView* view = MH_WAVEFORM_VIEW(obj);
	G_OBJECT_CLASS (mh_waveform_view_parent_class)->finalize(obj);
}


static void
mh_waveform_view_destroy (GtkObject* obj)
{
	MhWaveformView* view = MH_WAVEFORM_VIEW(obj);

	if (view->doc) {
		gtk_signal_disconnect_by_data(GTK_OBJECT(view->doc), view);
		gtk_object_unref(GTK_OBJECT(view->doc));
		view->doc = NULL;
	}

	GTK_OBJECT_CLASS(mh_waveform_view_parent_class)->destroy(obj);
}


/*
 *  Returns the underlying WaveformContext that the MhWaveformView is using.
 */
WaveformContext*
mh_waveform_view_get_canvas (MhWaveformView* view)
{
	g_return_val_if_fail(view, NULL);
	return view->priv->context;
}


static WaveformActor*
mh_waveform_add_actor (MhWaveformView* view, MhWaveformViewPart* part)
{
	MhWaveformViewPrivate* v = view->priv;
	Datasource* src = part->data->ds;

	if (src->type == DATASOURCE_CONVERT && src->data.clone->type == DATASOURCE_TEMPFILE)
		src = src->data.clone;
	if (src->type == DATASOURCE_CONVERT && src->data.clone->type == DATASOURCE_REAL)
		pwarn("FIXME DATASOURCE_REAL");

	Waveform* waveform = wavepool_lookup(src->data.virtual.filename);
	if(!waveform) waveform = wavepool_add(src->data.virtual.filename);

	part->actor = wf_context_add_new_actor(v->context, waveform);
	agl_actor__add_child(v->root, (AGlActor*)part->actor);
	wf_actor_set_colour(part->actor, 0x66ee66aa);
	((AGlActor*)part->actor)->region.y1 = BUTTON_SIZE;

#ifdef AGL_DEBUG_ACTOR
	static int i = 0;
	((AGlActor*)part->actor)->name = g_strdup_printf("Part %i", i++);

	// stagger each part for debugging
	//((AGlActor*)part->actor)->region.y1 = 4 * i;
#endif

	return part->actor;
}


static int
mh_waveform_view_get_width (MhWaveformView* view)
{
	GtkWidget* widget = (GtkWidget*)view;

	return GTK_WIDGET_REALIZED(widget) ? widget->allocation.width : 256;
}


	static void mh_waveform_view_update_part_list (MhWaveformView* view)
	{
		Document* d = view->doc;
		MhWaveformViewPrivate* v = view->priv;

		int n_chunk_parts = g_list_length(d->chunk->parts);
		int n_view_parts = g_list_length(v->parts);

		{
			GList* l = d->chunk->parts;
			GList* m = v->parts;
#ifdef DEBUG
			guint64 s = 0;
#endif
			for(;l&&m;l=l->next,m=m->next){
				DataPart* part = l->data;
				MhWaveformViewPart* vpart = m->data;
				//dbg(0, "  updating part: %5Lu: len=%5Li srcpos=%5Lu offset=%5Li x=%5i", s, part->position, part->length, vpart->source_to_view, calc_x_source_unbound(view, part->position, mh_waveform_view_get_width(view)));
				vpart->data = part;
#ifdef DEBUG
				s += part->length;
#endif
			}
		}

		if(n_chunk_parts != n_view_parts){
			dbg(0, "list length changed %i --> %i", n_view_parts, n_chunk_parts);

			if(n_view_parts < n_chunk_parts){
				int i; for(i=n_view_parts;i<n_chunk_parts;i++){
					dbg(0, "   adding part...");
					MhWaveformViewPart* part = AGL_NEW(MhWaveformViewPart,
						.data = g_list_nth_data(d->chunk->parts, i)
					);

					if(v->context){
						WaveformActor* actor = mh_waveform_add_actor(view, part);
					}

					v->parts = g_list_append(v->parts, part);
				}
			}else{
				for(int i=n_chunk_parts;i<n_view_parts;i++){
					dbg(1, "removing part %i...", i);

					GList* last = g_list_last(v->parts);
					v->parts = g_list_remove_link(v->parts, last);

					MhWaveformViewPart* part = last->data;
					agl_actor__remove_child(v->root, (AGlActor*)part->actor);
					g_list_free_1(last);
				}
			}
		}

		typedef struct {
			guint64 start, end;
		} Range;

		if(v->context){
			guint64 source_to_view = 0;
			guint64 s = 0;
			guint64 p = 0;
			Datasource* prev_src = NULL;
			GList* l = v->parts;
			for(;l;l=l->next){
				MhWaveformViewPart* part = l->data;
				DataPart* dpart = part->data;
				if(dpart->ds != prev_src){
					s = 0;
					prev_src = dpart->ds;
				}

				part->source_to_view = -(dpart->position - s);
				part->canvas_position.frames = p;
				s += dpart->length;
				p += dpart->length;
				dbg(1, "position=%Lu offset=%Li", dpart->position, part->source_to_view);

				if(!part->actor){
					dbg(1, "adding actor...");
					mh_waveform_add_actor(view, part);
				}

				//dbg(0, "setting region... %Lu --> %Lu", part->data->position, part->data->position + part->data->length);

				//note: when setting the source region, do not take the view into account.
				//      no! wrong, the region is modified for zooming and translation

				Range src = {
					.start = part->data->position,
					.end = part->data->position + part->data->length
				};
				Range rview = {
					.start = part->canvas_position.frames,
					.end = src.end + part->canvas_position.frames
				};

				if(part->canvas_position.frames >= d->viewend){
					dbg(0, "part outside viewport (rhs). start=%Lu end=%Lu", rview.start, rview.end);
					part->show = false;
					((AGlActor*)part->actor)->region.x2 = -1;
				}else if(rview.end < d->viewstart){
					part->show = false;
					dbg(0, "part outside viewport (lhs). start=%Lu end=%Lu viewstart=%Lu", rview.start, rview.end, d->viewstart);
					wf_actor_set_region(part->actor, &(WfSampleRegion){0, 256});
				}else{
					part->show = true;
					mh_waveform_view_set_region(view, part);

					// this is going to be done again later, but in a timeout handler which is too late.
					_mh_waveform_view_allocate_actor (view, part);
				}

				_mh_waveform_view_set_actor(view, part);
			}
		}

		if(!v->grid){
			WaveformActor* wf_actor = ((MhWaveformViewPart*)v->parts->data)->actor;
			agl_actor__add_child(v->root, v->grid = grid_actor(wf_actor));
			v->grid->colour = 0xffff00ff;

			// Move the grid underneath the toolbar
			v->root->children = g_list_remove(v->root->children, v->grid);
			v->root->children = g_list_insert_before(v->root->children, g_list_find(v->root->children, v->toolbar), v->grid);

			agl_actor__add_child(v->root, v->ruler = ruler_actor(wf_actor));

			agl_actor__add_child(v->root, v->labels = labels_actor(v->context));
			v->labels->colour = 0xffff0077;

			mh_waveform_view_on_change(view, CHANGE_DIM);
		}
	}


static gboolean
do_change (gpointer _view)
{
	GtkWidget* widget = _view;
	MhWaveformView* view = _view;
	MhWaveformViewPrivate* v = view->priv;

	if(v->change & CHANGE_DIM){
		v->root->region.x2 = widget->allocation.width;
		v->root->region.y2 = widget->allocation.height;

		wf_context_set_scale(v->context, ((float)(view->doc->viewend - view->doc->viewstart)) / agl_actor__width(v->root));

		GList* l = v->parts;
		for(;l;l=l->next){
			_mh_waveform_view_allocate_actor (view, (MhWaveformViewPart*)l->data);
		}

		v->root->region = v->grid->region = (AGlfRegion){
			.x1 = 0,
			.y2 = 0,
			.x2 = widget->allocation.width,
			.y2 = widget->allocation.height,
		};

		v->root->scrollable = (AGliRegion){
			.x1 = 0,
			.y2 = 0,
			.x2 = widget->allocation.width,
			.y2 = widget->allocation.height,
		};

		v->toolbar->region = (AGlfRegion){
			.x2 = widget->allocation.width,
			.y2 = BUTTON_SIZE + 2
		};

		int ruler_height = 20;
		v->ruler->region = (AGlfRegion){
			.x1 = 0,
			.y1 = widget->allocation.height - ruler_height,
			.x2 = widget->allocation.width,
			.y2 = widget->allocation.height
		};

		v->labels->region = (AGlfRegion){
			.x1 = 0,
			.y1 = widget->allocation.height - ruler_height,
			.x2 = widget->allocation.width,
			.y2 = widget->allocation.height - ruler_height + 10
		};

		v->change &= ~CHANGE_DIM;
	}

	return v->update = G_SOURCE_REMOVE;
}


static void
mh_waveform_view_on_change (MhWaveformView* view, ChangeType change)
{
	MhWaveformViewPrivate* v = view->priv;

	v->change |= change;

	if(v->change && !v->update) v->update = g_timeout_add(50, do_change, view);
}


static void
mh_waveform_view_on_document_change (Document* d, gpointer user_data)
{
	MhWaveformView* view = user_data;
	MhWaveformViewPrivate* v = view->priv;

	//TODO if zoom has changed, d->viewstart and d->viewend will have changed.

	dbg(0, "viewstart=%Lu viewend=%Lu", d->viewstart, d->viewend);
	dbg(0, "---------------------------------");

	v->context->sample_rate = d->chunk->format.samplerate;

#if 0
	dbg(0, "chunk=%p parts=%p", d->chunk, d->chunk->parts);
	if(g_list_length(d->chunk->parts) > 1) dbg(0, "** multiple parts (n=%i)", g_list_length(d->chunk->parts));

	GList* l = d->chunk->parts;
	for(;l;l=l->next){
		DataPart* part = l->data;
		dbg(0, "   position=%5Lu len=%5Li", part->position, part->length);
	}

	GList* m = d->chunk->parts;
	for(;m;m=m->next){
		MhWaveformViewPart* vpart = m->data;
		DataPart* part = vpart->data;
		printf("%s:   position=%5Lu len=%5Li\n", __func__, part->position, part->length);
	}

	// has the wav file changed ?
	if(!d->history_pos){
		dbg(0, "no history");
	} else {
		dbg(0, "history=%p", d->history_pos);
	}
#endif

	mh_waveform_view_update_part_list(view);

	if(d->cursorpos != d->old_cursorpos){
	}

	if(d->viewstart != v->old_viewstart || d->viewend != v->old_viewend){
		mh_waveform_view_on_change(view, CHANGE_DIM);
	}else{
		pwarn("viewport NOT changed"); // do we ever get here?
	}

	//gtk_widget_queue_draw(GTK_WIDGET(view));
}


static void
mh_waveform_view_on_selection_change (Document* d, gpointer user_data)
{
	MhWaveformView* view = user_data;

	/* Calculate the user-visible part of the old selection */
	off_t os = MAX(d->viewstart, d->old_selstart);
	off_t oe = MIN(d->viewend,   d->old_selend);
     
	/* Calculate user-visible part of the new selection */
	off_t ns = MAX(d->viewstart, d->selstart);
	off_t ne = MIN(d->viewend,   d->selend);

	/* Different cases:
	 *  1. Neither the old nor the new selection is in view.
	 *  2. The old selection wasn't in view but the new is..
	 *  3. The new selection is in view but the old wasn't..
	 *  4. Both are in view and overlap
	 *  5. Both are in view and don't overlap
	 */

	if (os >= oe && ns >= ne) {
		/* Case 1 - Do nothing */
	} else if (os >= oe) {
		/* Case 2 - Draw the entire new sel. */
		mh_waveform_view_redraw_range(view, ns, ne);
	} else if (ns >= ne) {
		/* Case 3 - Redraw the old sel. */
		mh_waveform_view_redraw_range(view, os, oe);
	} else if ((ns >= os && ns < oe) || (os >= ns && os < ne)) {
		/* Case 4 - Redraw the changes on both sides */
		if (os != ns)
			mh_waveform_view_redraw_range(view, MIN(os,ns), MAX(os,ns));
		if (oe != ne)
			mh_waveform_view_redraw_range(view, MIN(oe,ne), MAX(oe,ne));
	} else {
		/* Case 5 - Redraw both selections */
		mh_waveform_view_redraw_range(view, os, oe);
		mh_waveform_view_redraw_range(view, ns, ne);
	}
}


static void
mh_waveform_view_on_cursor_change (Document* d, gboolean rolling, gpointer user_data)
{
	MhWaveformView* view = MH_WAVEFORM_VIEW(user_data);
	int curpix=0, oldpix=0;

	gboolean old_in_view = (d->old_cursorpos >= d->viewstart && d->old_cursorpos <  d->viewend);
	gboolean new_in_view = (d->cursorpos >= d->viewstart && d->cursorpos < d->viewend);
	if (old_in_view)
		oldpix = calc_x(view, d->old_cursorpos, GTK_WIDGET(view)->allocation.width);
	if (new_in_view)
		curpix = calc_x(view, d->cursorpos,GTK_WIDGET(view)->allocation.width);

	if (old_in_view && (!new_in_view || oldpix != curpix) || new_in_view && (!old_in_view || oldpix != curpix)){
		gtk_widget_queue_draw((GtkWidget*)view);
	}
}


void
mh_waveform_view_set_vscale (MhWaveformView* view, gfloat scale)
{
	dbg(0, "%.2f", scale);

	MhWaveformViewPrivate* v = view->priv;

	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* part = l->data;
		if(part->actor) wf_actor_set_vzoom(part->actor, scale);
	}
}


static void
mh_waveform_view_redraw_range (MhWaveformView* view, off_t start, off_t end)
{
	gtk_widget_queue_draw((GtkWidget*)view);
}


#if 0
static gint
mh_waveform_view_event(GtkWidget* widget, GdkEvent* event, gpointer _arrange)
{
	MhWaveformView* view = (MhWaveformView*)widget;
	Document* d = view->doc;

	gboolean handled = false;

	switch (event->type){
		case GDK_ENTER_NOTIFY:
			break;
		case GDK_BUTTON_PRESS:
			dbg (0, "GDK_BUTTON_PRESS");
			gtk_widget_grab_focus(widget);
     		off_t fr = calc_sample(view, event->button.x, (gfloat)widget->allocation.width);
			switch(event->button.button){
				case 1:
					break;
				case 3:
					//right click
					document_set_cursor(d, fr);
					break;
				default:
					break;
			}
			handled = true;
			break;
		case GDK_MOTION_NOTIFY:
			break;
		case GDK_BUTTON_RELEASE:
			dbg (0, "GDK_BUTTON_RELEASE");
			break;
		case GDK_LEAVE_NOTIFY:
			break;
		case GDK_SCROLL:
			break;
	}
	return handled;
}
#endif


static gint
scroll_wheel (GtkWidget* widget, gdouble mouse_x, int direction)
{
	MhWaveformView* view = (MhWaveformView*)widget;
	Document* d = view->doc;

	gfloat zf;
	off_t ui1, ui2;
	if (d == NULL) return FALSE;
	dragstart = dragend = calc_sample(view, mouse_x, (gfloat)widget->allocation.width);

	if (direction == -1){	// Scroll wheel down
		if (d->viewend - d->viewstart == widget->allocation.width)
			return FALSE;
		zf = 1.0/1.4;
	}
	else if (d->viewend - d->viewstart < 2)
		zf = 4.0;           // Special case to avoid locking in maximum zoom
	else zf = 1.4;		    // Scroll wheel up

	ui1 = dragstart-(off_t)((gfloat)(dragstart-d->viewstart))*zf;
	ui2 = dragstart+(off_t)((gfloat)(d->viewend-dragstart))*zf;
	if (ui1 < 0) ui1 = 0;
	if (ui2 < dragstart || ui2 > d->chunk->length) ui2 = d->chunk->length;
	if (ui2 == ui1) {
		if (ui2 < d->chunk->length)
			ui2++;
		else if (ui1 > 0)
			ui1--;
	}
	document_set_view(d, ui1, ui2);
	return FALSE;
}

static gint
mh_waveform_view_button_press (GtkWidget* widget, GdkEventButton* event)
{
	MhWaveformView* view = (MhWaveformView*)widget;
	Document* d = view->doc;
	gdouble sp, ep;
	off_t o;
	if (d == NULL) return FALSE;
	if (event->type == GDK_2BUTTON_PRESS && event->button == 1) {
		o = calc_sample(view, event->x, (gfloat)widget->allocation.width);
		/* TODO
		gtk_signal_emit(GTK_OBJECT(view),
		chunk_view_signals[DOUBLE_CLICK_SIGNAL], &o);
		*/
		return FALSE;
	}

	if(event->y < BUTTON_SIZE)
		return FALSE;

	dragstart = dragend = calc_sample(view, event->x, (gfloat)widget->allocation.width);

	/* Snap to sel start/end */
	sp = (gdouble)calc_x(view, d->selstart, widget->allocation.width);
	ep = (gdouble)calc_x(view, d->selend, widget->allocation.width);
	if (fabs(event->x - sp) < 3.0)
		dragstart = dragend = d->selstart;
	else if (fabs(event->x - ep) < 3.0)
		dragstart = dragend = d->selend;

	if ((event->state & GDK_SHIFT_MASK) != 0 && d->selend != d->selstart) {

		dragmode = TRUE;

		/* The right selection endpoint is nearest to dragstart
		* <=> dragstart > (selstart+selend)/2
		* <=> 2*dragstart > selstart+selend
		* <=> dragstart+dragstart > selstart+selend
		* <=> dragstart-selend > selstart-dragstart
		* This should be overflow-safe for large values 
		*/
		if ((event->button == 1 && dragstart-d->selend > d->selstart-dragstart) || (event->button != 1 && dragstart-d->selend < d->selstart-dragstart)){
			/* Drag right endpoint */
			dragstart = d->selstart;
		} else {
			/* Drag left endpoint */
			dragstart = d->selend;
		}
		document_set_selection(d, dragstart, dragend);

	} else if (event->button == 1 || event->button == 2){
		dragmode = TRUE;
		if (d->selend != d->selstart){
			if (d->selstart >= d->viewstart && dragstart == d->selstart){
				dragstart = d->selend;
				dragend = d->selstart;
				return FALSE;
			} else if (d->selend <= d->viewend && dragstart == d->selend){
				dragstart = d->selstart;
				dragend = d->selend;
				return FALSE;
			}
		}
		document_set_selection(d, dragstart, dragend);
	} else if (event->button == 3){
		document_set_cursor(d,dragstart);
	} else if (event->button == 4 || event->button == 5){
		if (event->button == 5) scroll_wheel(widget, event->x, -1);
		else                    scroll_wheel(widget, event->x, 1);
	}
	return FALSE;
}


static gint
mh_waveform_view_motion_notify (GtkWidget* widget, GdkEventMotion* event)
{
	MhWaveformView* view = (MhWaveformView*)widget;
	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	if (agl_actor__on_event((AGlScene*)v->root, (GdkEvent*)event)) return AGL_HANDLED;

	gint x1, x2;
	static GdkCursor* arrows = NULL;
	if (!d) return FALSE;

	if (dragmode) {
		if (event->x < widget->allocation.width)
			dragend = calc_sample(view, (event->x > 0.0) ? event->x : 0.0, (gfloat)widget->allocation.width);
		else
			dragend = d->viewend;

		document_set_selection(d, dragstart, dragend);
		if (event->x < 0.0 || event->x > widget->allocation.width){
			if (!autoscroll) {
				autoscroll = TRUE;
				autoscroll_view = view;
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
				g_get_current_time(&autoscroll_start);
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
			}
			if (event->x < 0.0) autoscroll_amount = event->x;
			else autoscroll_amount = event->x - widget->allocation.width;

			chunk_view_autoscroll(NULL,&autoscroll_start,NULL);
		} else {
			autoscroll = FALSE;
		}
	} else if (d->selstart != d->selend) {
		if (d->selstart >= d->viewstart) 
			x1 = calc_x(view, d->selstart, widget->allocation.width);
		else
			x1 = -500;

		if (d->selend <= d->viewend)
			x2 = calc_x(view, d->selend, widget->allocation.width);
		else
			x2 = -500;

		if (fabs(event->x-(double)x1)<3.0 || fabs(event->x-(double)x2)<3.0) {
			if (arrows == NULL)
				arrows = gdk_cursor_new(GDK_SB_H_DOUBLE_ARROW);
			gdk_window_set_cursor(widget->window,arrows);
		} else {
			gdk_window_set_cursor(widget->window, NULL);
		}
	}
	return FALSE;
}


static gint
mh_waveform_view_leave_notify (GtkWidget* widget, GdkEventCrossing* event)
{
}


static AGlActor*
extras_actor (WaveformActor* wa)
{
	void extras__gl1_set_state (AGlActor* actor)
	{
		glDisable(GL_TEXTURE_2D);
		glLineWidth(1);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void extras__init (AGlActor* actor)
	{
		if(!agl->use_shaders){
			actor->set_state = extras__gl1_set_state;
		}
	}

	bool extras__paint (AGlActor* actor)
	{
		MhWaveformView* view = actor->root->user_data;
		MhWaveformViewPrivate* v = view->priv;
		GtkWidget* widget = (GtkWidget*)view;
		Document* d = view->doc;

#if 0
		if(have_shaders){
			agl_use_program((AGlShader*)agl->shaders.plain);
		}
#endif
		// selection
		if (d->selstart != d->selend) {
			if (agl->use_shaders) {
				PLAIN_COLOUR2 (agl->shaders.plain) = 0xff1919bb;
				agl->shaders.plain->set_uniforms_(agl->shaders.plain);
			} else {
				glColor4f(1.0, 0.1, 0.1, 0.7);
			}
			int x1 = calc_x(view, d->selstart, widget->allocation.width);
			int x2 = calc_x(view, d->selend, widget->allocation.width);
			agl_rect(x1, BUTTON_SIZE, x2 - x1, widget->allocation.height);
		}

		// cursor
		if (agl->use_shaders) {
			PLAIN_COLOUR2 (agl->shaders.plain) = 0xffffff7f;
			agl->shaders.plain->set_uniforms_(agl->shaders.plain);
		} else {
			glColor4f(1.0, 1.0, 1.0, 0.5);
		}
		int x = calc_x(view, d->cursorpos, widget->allocation.width);
		agl_rect(x, 0.0, 1, widget->allocation.height);

		// centre line
		glColor4f(1.0, 1.0, 1.0, 0.15);
		int n_channels = d->chunk->format.channels;
		for (int c=0;c<n_channels;c++) {
			agl_rect(0, BUTTON_SIZE + (widget->allocation.height - BUTTON_SIZE - BOTTOM_BORDER)/ (2 * n_channels) + (c * (widget->allocation.height - BUTTON_SIZE - BOTTOM_BORDER)) / 2, widget->allocation.width, 1);
		}

		return true;
	}

	AGlActor* extras = agl_actor__new(AGlActor,
		.init = extras__init,
	);
	extras->paint = extras__paint;
	extras->program = (AGlShader*)agl->shaders.plain;
	extras->name = "Extras";
	extras->region.x2 = extras->region.y2 = 1;

	return extras;
}


#if 0
static void
draw(MhWaveformView* view)
{
	Waveform* wf = view->waveform;
	GtkWidget* widget = (GtkWidget*)view;
	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	gboolean have_shaders = agl->use_shaders;
	if(!have_shaders){
		glNormal3f(0, 0, 1);
		glDisable(GL_TEXTURE_2D);
		glLineWidth(1);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	if(!wf) return;

	agl_actor__paint((AGlActor*)v->root);
}
#endif


#define FLOAT(x) ((gfloat)(x))

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
static int
chunk_view_autoscroll (gpointer timesource, GTimeVal *current_time, gpointer user_data)
{
	GTimeVal diff,new_time;
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
	gfloat smp;
	off_t ismp;

	/* puts("chunk_view_autoscroll"); */

	g_assert(timesource == NULL || timesource == autoscroll_source);

	if (!autoscroll) return 0;
	timeval_subtract(&diff, current_time, &autoscroll_start);

	/* printf("diff: %d,%d\n",diff.tv_sec,diff.tv_usec); */
	if (diff.tv_sec > 0 || diff.tv_usec > 10000) {

		memcpy(&autoscroll_start, current_time, sizeof(autoscroll_start));

		/* Convert diff -> smp */
		smp = 0.2 * FLOAT(diff.tv_usec/10000 + diff.tv_sec*100) * autoscroll_amount * FLOAT(autoscroll_view->doc->viewend-autoscroll_view->doc->viewstart)/ FLOAT(GTK_WIDGET(autoscroll_view)->allocation.width);
		/* Convert smp->ismp, update view and selection */
		if(smp < 0.0){
			ismp = (off_t)(-smp);
			if (ismp > dragend) {
				ismp = dragend;
				autoscroll = FALSE;
			}
			dragend -= ismp;
			document_set_selection(autoscroll_view->doc,dragend,dragstart);
			document_set_view(autoscroll_view->doc, dragend,
			autoscroll_view->doc->viewend-ismp);
		}else{
			ismp = (off_t)smp;
			if (dragend+ismp > autoscroll_view->doc->chunk->length) {
				ismp = autoscroll_view->doc->chunk->length - dragend;
				autoscroll = FALSE;
			}
			dragend += ismp;
			document_set_selection(autoscroll_view->doc,dragstart,dragend);
			document_set_view(autoscroll_view->doc, autoscroll_view->doc->viewstart+ismp, dragend);
		}
	} 

	if(autoscroll){
		memcpy(&new_time,current_time,sizeof(new_time));
		new_time.tv_usec = ((new_time.tv_usec / 25000) + 1) * 25000;
		if (new_time.tv_usec >= 1000000){
			new_time.tv_sec += 1;
			new_time.tv_usec -= 1000000;
		}
//TODO autoscroll_source should be integer not pointer?
		if (autoscroll_source == NULL)
			autoscroll_source = GINT_TO_POINTER(mainloop_time_source_add(&new_time, chunk_view_autoscroll, NULL));
		else
			mainloop_time_source_restart(autoscroll_source, &new_time);
	}

	return 0;
}

static gint
mh_waveform_view_button_release (GtkWidget* widget, GdkEventButton* event)
{
	MhWaveformView* view = (MhWaveformView*)widget;
	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	if (event->button == 1) {
		if (agl_actor__on_event((AGlScene*)v->root, (GdkEvent*)event)) return AGL_HANDLED;
	}

	if (event->button == 2 && d != NULL) {
		document_play_selection(d, false, 1.0);
	}
	autoscroll = FALSE;
	dragmode = FALSE;
	return FALSE;
}


#if 0 //XXXXX
static void chunk_view_class_init(GtkObjectClass *klass)
{
     GtkWidgetClass *wc = GTK_WIDGET_CLASS(klass);
     ChunkViewClass *cvc = CHUNKVIEW_CLASS(klass);
     parent_class = gtk_type_class( gtk_drawing_area_get_type() );

     wc->expose_event = chunk_view_expose;
     wc->size_request = chunk_view_size_request;
     wc->size_allocate = chunk_view_size_allocate;
     wc->button_press_event = chunk_view_button_press;
     wc->motion_notify_event = mh_waveform_view_motion_notify;
     wc->button_release_event = mh_waveform_view_button_release;
#if GTK_MAJOR_VERSION == 2
     wc->scroll_event = chunk_view_scrollwheel;
#endif
     cvc->double_click = NULL;

     chunk_view_signals[DOUBLE_CLICK_SIGNAL] = 
         gtk_signal_new("double-click", GTK_RUN_FIRST, GTK_CLASS_TYPE(klass),
                        GTK_SIGNAL_OFFSET(ChunkViewClass,double_click),
                        gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
			GTK_TYPE_POINTER );

     gtk_object_class_add_signals(klass,chunk_view_signals,LAST_SIGNAL);
}
#endif

static off_t
calc_sample (MhWaveformView* view, gfloat x, gfloat xmax)
{
	return view->doc->viewstart + (x/xmax) * (view->doc->viewend - view->doc->viewstart);
}


static gint
calc_x (MhWaveformView* view, off_t sample, off_t width)
{
	Document* d = view->doc;

	if (sample < d->viewstart)
		return -1;
	if (sample > d->viewend)
		return width;
	else {
		float f = sample - d->viewstart;
		float g = d->viewend - d->viewstart;
		//dbg(0, "sample=%Lu x=%.2f/%.2f * %Lu", sample, f, g, width);
		return width * (f/g);
	}
}

static gint
calc_x_unbounded (MhWaveformView* view, off_t sample, off_t width)
{
	Document* d = view->doc;

	float f = sample - d->viewstart;
	float g = d->viewend - d->viewstart;
	//dbg(0, "sample=%Lu x=%.2f/%.2f * %Lu", sample, f, g, width);
	return width * (f/g);
}

static gint
calc_x_relative (MhWaveformView* view, off_t sample, off_t width)
{
	Document* d = view->doc;

	float f = sample;
	float g = d->viewend - d->viewstart;
	//dbg(0, "sample=%Lu x=%.2f/%.2f * %Lu", sample, f, g, width);
	return width * (f/g);
}

static gint
calc_x_source_unbound (MhWaveformView* view, off_t sample, off_t width)
{
	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	MhWaveformViewPart* p = NULL;
	int i = 0;
	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* vpart = l->data;
		DataPart* part = vpart->data;

		guint64 pos = part->position;// + vpart->source_to_view;
		p = vpart;
		if((sample >= pos) && (sample < pos + part->length)) break;

		i++;
	}
	g_return_val_if_fail(p, 0);

	//float f = sample + p->source_to_view - d->viewstart;
	float f = p->canvas_position.frames + sample - d->viewstart;
	float g = d->viewend - d->viewstart;
	if(g < 0.0){
		dbg(0, "error! start=%Lu end=%Lu", d->viewstart, d->viewend);
		g_return_val_if_fail(!(g < 0.0), 0);
	}
	return width * (f/g);
}

/*
 *  Translates a model coordinate in frames to the pixel value relative to the window left hand side.
 */
static gint
calc_x_for_part (MhWaveformView* view, MhWaveformViewPart* vpart, off_t sample, off_t width)
{
	// Changed to specifying the part directly in order to differentate two identical parts.
	// This occurs when a whole part is copy n pasted.

	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	MhWaveformViewPart* p = vpart;

	float f = p->canvas_position.frames + sample - d->viewstart;
	float g = d->viewend - d->viewstart;

	return width * (f/g);
}

#if 0
static gint
calc_x_source(MhWaveformView* view, off_t sample, off_t width)
{
#if 0
	// uses 'source' frames, so takes into account cut regions.

	// will it work when have pasted regions also? probably not.

	//TODO make it work when zoomed in

	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	off_t total_frames = 0;
	off_t missing_frames = 0;
	//off_t total_missing_frames = 0;
	off_t s = 0;
	MhWaveformViewPart* p = NULL;
	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* vpart = l->data;
		DataPart* part = vpart->data;
		off_t position = part->position;
		off_t length = part->length;
		total_frames += length;

		p = vpart;
		if(sample >= part->position){
			if(position > s){
				missing_frames += position - s;
//if(missing_frames != -vpart->source_to_view) dbg(0, "!!! xlate error");
			}
			//dbg(0, ">> -");
		}
		else {
			//dbg(0, ">> breaking at %Lu missing=%Lu", s, missing_frames);
			break;
		}
		//total_missing_frames += position - s;
		s = position + length;
	}
	//dbg(0, "total_frames=%Lu missing_frames=%Lu", total_frames, missing_frames);

	if (sample < d->viewstart)
		return -1;
	if (sample - missing_frames > d->viewend)
		return width;
	else {
		missing_frames = -p->source_to_view;
		float f = sample - missing_frames - d->viewstart;
		//float f = sample + p->source_to_view - d->viewstart;
		float g = d->viewend - d->viewstart;
		if(g<0.0){
			dbg(0, "error! start=%Lu end=%Lu", d->viewstart, d->viewend);
			g_return_if_fail(!(g<0.0));
		}
		//dbg(0, "sample=%Lu x=%.2f/%.2f * %Lu", sample, f, g, width);
		return width * (f/g);
	}
#else
	MhWaveformViewPrivate* v = view->priv;
	Document* d = view->doc;

	off_t missing_frames = 0;
	off_t s = 0;
	MhWaveformViewPart* p = NULL;
	GList* l = v->parts;
	for(;l;l=l->next){
		MhWaveformViewPart* vpart = l->data;
		DataPart* part = vpart->data;
		off_t position = part->position;
		off_t length = part->length;
		//total_frames += length;

		/*
		if(sample >= part->position){
			if(position > s){
				//missing_frames += position - s;
				//if(missing_frames != -vpart->source_to_view) dbg(0, "!!! xlate error");
			}
		}
		else {
			//dbg(0, ">> breaking at %Lu missing=%Lu", s, missing_frames);
			break;
		}
		*/
		if(sample < part->position) break;
		p = vpart;

		//total_missing_frames += position - s;
		s = position + length;
	}
	missing_frames = -p->source_to_view;

	if (sample < d->viewstart)
		return -1;
	if (sample - missing_frames > d->viewend)
		return width;
	else
		return calc_x_source_unbound(view, sample, width);
#endif
}
#endif

