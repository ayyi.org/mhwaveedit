/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "waveform/waveform.h"

GHashTable* wavepool = NULL;


Waveform*
wavepool_lookup (const char* filename)
{
	return g_hash_table_lookup(wavepool, filename);
}


Waveform*
wavepool_add (const char* filename)
{
	Waveform* waveform = g_object_ref(waveform_new(filename));

	g_hash_table_insert(wavepool, (char*)filename, waveform);

	return waveform;
}


void
wavepool_remove (Waveform* waveform)
{
}
