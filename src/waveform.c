/*
 * Copyright (C) 2012-2013, Tim Orford
 *
 * This file is part of mhWaveEdit.
 *
 * mhWaveEdit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or  
 * (at your option) any later version.
 *
 * mhWaveEdit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mhWaveEdit; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <config.h>
#include <waveform/view.h>
#include "document.h"
#include "chunkview.h"

typedef struct {
	Document* doc;
} V;
V _v = {NULL};

static void chunk_view_changed                   (Document*, gpointer);
static void mh_waveform_view_on_selection_change (Document*, gpointer);
static void mh_waveform_view_on_cursor_change    (Document*, gboolean rolling, gpointer);
static void mh_waveform_view_redraw              ();


#if 0
void
mh_waveform_view_set_document(WaveformView* view, Document* doc)
{
	printf("%s\n", __func__);
	V* v = &_v;
	wf_debug = 1;

	if (v->doc == doc) return;
	if (v->doc) {
		g_signal_handlers_disconnect_matched(GTK_OBJECT(v->doc), G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, view);
		g_object_unref(GTK_OBJECT(v->doc));
	}
	v->doc = doc;
	if (doc) { 
		gtk_object_ref(GTK_OBJECT(doc)); 
		gtk_object_sink(GTK_OBJECT(doc)); 
		g_signal_connect(GTK_OBJECT(doc), "view_changed",      GTK_SIGNAL_FUNC(chunk_view_changed), view);
		g_signal_connect(GTK_OBJECT(doc), "state_changed",     GTK_SIGNAL_FUNC(chunk_view_changed), view);
		g_signal_connect(GTK_OBJECT(doc), "selection_changed", GTK_SIGNAL_FUNC(mh_waveform_view_on_selection_change), view);
		g_signal_connect(GTK_OBJECT(doc), "cursor_changed",    GTK_SIGNAL_FUNC(mh_waveform_view_on_cursor_change), view);
	}

	waveform_view_load_file(view, doc->filename);

	chunk_view_changed(v->doc, view);
}


static void
chunk_view_changed(Document* d, gpointer user_data)
{
	//printf("filename=%s\n", d->filename);
	printf("%s: viewstart=%Lu viewend=%Lu\n", __func__, d->viewstart, d->viewend);
	//printf("n_parts=%i\n", g_list_length(d->chunk->parts));
	if(g_list_length(d->chunk->parts) > 1) printf("%s: TODO multiple parts (n=%i)\n", __func__, g_list_length(d->chunk->parts));
	GList* l = d->chunk->parts;
	for(;l;l=l->next){
		DataPart* part = l->data;
		printf("%s: position=%Lu\n", __func__, part->position);
	}

	WaveformView* view = user_data;
	waveform_view_set_region(view, d->viewstart, d->viewend);

	//gtk_widget_queue_draw(GTK_WIDGET(view));
}


static void
mh_waveform_view_on_selection_change(Document* d, gpointer user_data)
{
	printf("%s\n", __func__);
	ChunkView* cv = CHUNKVIEW(user_data);

	/* Calculate the user-visible part of the old selection */
	off_t os = MAX(d->viewstart, d->old_selstart);
	off_t oe = MIN(d->viewend,   d->old_selend);
     
	/* Calculate user-visible part of the new selection */
	off_t ns = MAX(d->viewstart, d->selstart);
	off_t ne = MIN(d->viewend,   d->selend);

	/* Different cases:
	 *  1. Neither the old nor the new selection is in view.
	 *  2. The old selection wasn't in view but the new is..
	 *  3. The new selection is in view but the old wasn't..
	 *  4. Both are in view and overlap
	 *  5. Both are in view and don't overlap
	 */

	if (os >= oe && ns >= ne) {
		/* Case 1 - Do nothing */
	} else if (os >= oe) {
		/* Case 2 - Draw the entire new sel. */
		mh_waveform_view_redraw(cv, ns, ne);
	} else if (ns >= ne) {
		/* Case 3 - Redraw the old sel. */
		mh_waveform_view_redraw(cv, os, oe);
	} else if ((ns >= os && ns < oe) || (os >= ns && os < ne)) {
		/* Case 4 - Redraw the changes on both sides */
		if (os != ns)
			mh_waveform_view_redraw(cv,MIN(os,ns),MAX(os,ns));
		if (oe != ne)
			mh_waveform_view_redraw(cv,MIN(oe,ne),MAX(oe,ne));
	} else {
		/* Case 5 - Redraw both selections */
		mh_waveform_view_redraw(cv, os, oe);
		mh_waveform_view_redraw(cv, ns, ne);
	}
}


static void
mh_waveform_view_on_cursor_change(Document* d, gboolean rolling, gpointer user_data)
{
	printf("%s\n", __func__);
#if 0
	ChunkView *cv = CHUNKVIEW(user_data);
	gboolean old_in_view,new_in_view;
	int curpix=0,oldpix=0;

	old_in_view = (d->old_cursorpos >= d->viewstart && d->old_cursorpos <  d->viewend);
	new_in_view = (d->cursorpos >= d->viewstart && d->cursorpos < d->viewend);
	if (old_in_view)
		oldpix = calc_x(cv,d->old_cursorpos, GTK_WIDGET(cv)->allocation.width);
	if (new_in_view)
		curpix = calc_x(cv,d->cursorpos,GTK_WIDGET(cv)->allocation.width);
	if (old_in_view && (!new_in_view || oldpix != curpix))
		gtk_widget_queue_draw_area(GTK_WIDGET(cv),oldpix,0,1, cv->image_height);
	if (new_in_view && (!old_in_view || oldpix != curpix))
		gtk_widget_queue_draw_area(GTK_WIDGET(cv),curpix,0,1, cv->image_height);
#endif
}


void
mh_waveform_view_set_vscale(WaveformView* view, gfloat scale)
{
	printf("%s: %.2f\n", __func__, scale);

	//TODO as actor is private, we must add a fn to WaveformView
	//if(view->priv->actor) wf_actor_set_vzoom(view->priv->actor, scale);
}


static void
mh_waveform_view_redraw()
{
}
#endif


