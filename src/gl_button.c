/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

typedef void      (*ButtonAction)   (AGlActor*, gpointer);
typedef bool      (*ButtonGetState) (AGlActor*, gpointer);
typedef void      (*ButtonTooltip)  (AGlActor*, char*);

typedef struct {
    AGlActor       actor;
    guint          icon;
    ButtonAction   action;
    ButtonGetState get_state;
    ButtonTooltip  tooltip;
    float          bg_opacity;
    WfAnimatable** animatables;
    gpointer       user_data;
} ButtonActor;

#define BUTTON_SIZE 20
#define BUTTON_PADDING 2

#define IS_HOVERED(B) ((B)->bg_opacity > 0.0)

static bool button_on_event (AGlActor*, GdkEvent*, AGliPt);
static void button_free     (AGlActor*);

static AGlActorClass actor_class = {0, "Button", NULL, button_free};


uint32_t
color_gdk_to_rgba (GdkColor* color)
{
	return ((color->red / 0x100) << 24) + ((color->green / 0x100) << 16) + ((color->blue / 0x100) << 8) + 0xff;
}


static uint32_t
get_style_bg_color_rgba (GtkWidget* widget, GtkStateType state)
{
	GtkStyle* style = gtk_widget_get_style(widget);

	GdkColor bg = style->bg[state];
	bg.red   = MIN((1 << 16) - 1, bg.red);
	bg.green = MIN((1 << 16) - 1, bg.green);
	bg.blue  = MIN((1 << 16) - 1, bg.blue);

	return color_gdk_to_rgba(&bg);
}


AGlActor*
gl_button (int icon, ButtonAction action, ButtonGetState get_state, gpointer user_data)
{
	bool gl_button_paint (AGlActor* actor)
	{
		ButtonActor* button = (ButtonActor*)actor;

		bool state = button->get_state ? button->get_state(actor, button->user_data) : false;

		AGlRect r = {
			.w = agl_actor__width(actor),
			.h = agl_actor__height(actor)
		};

		// background
		if (state) {
			PLAIN_COLOUR2 (agl->shaders.plain) = get_style_bg_color_rgba (actor->root->gl.gdk.widget, GTK_STATE_SELECTED);
			agl_use_program (agl->shaders.plain);
			agl_rect_(r);
		}
		// hover background
		if (/*!agl_actor__is_disabled(button) &&*/ IS_HOVERED(button)) { // dont get events if disabled, so no need to check state (TODO turn off hover when disabling).
			int alpha = button->bg_opacity * (256 - 1);
			// TODO using the prelight colour will only work if the background is using the gtk background colour.
#if 0
			agl->shaders.plain->uniform.colour = (get_style_bg_color_rgba(actor->root->widget, GTK_STATE_PRELIGHT) & 0xffffff00) + alpha;
#else
			PLAIN_COLOUR2 (agl->shaders.plain) = 0x77777700 + alpha;
#endif
			agl_use_program (agl->shaders.plain);
			agl_rect_ (r);
		}

		// icon
		agl->shaders.texture->uniform.fg_colour = 0xffffff00 + (agl_actor__is_disabled(actor) ? 0x77 : 0xff);
		agl_use_program((AGlShader*)agl->shaders.texture);

		agl_textured_rect(bg_textures[button->icon + (state ? 1 : 0)], BUTTON_PADDING, BUTTON_PADDING, r.w - 2 * BUTTON_PADDING, r.h - 2 * BUTTON_PADDING, NULL);

		return true;
	}

	bool gl_button_paint_gl1 (AGlActor* actor)
	{
		ButtonActor* button = (ButtonActor*)actor;

		bool state = button->get_state ? button->get_state(actor, button->user_data) : false;

		AGlRect r = {
			.x = 0.0,
			.y = 0.0,
			.w = agl_actor__width(actor),
			.h = agl_actor__height(actor->parent)
		};

		glDisable(GL_TEXTURE_2D);
		if(state){
			//agl_colour_rbga(get_style_bg_color_rgba(actor->root->gl.gdk.widget, GTK_STATE_SELECTED));
			agl_rect(r.x, r.y, r.w + 1, r.h + 1);
		}
		if(!actor->disabled && agl_actor__is_hovered(actor)){
			glColor4f(1.0, 1.0, 1.0, 0.1);
			agl_rect_(r);
		}

		glEnable(GL_TEXTURE_2D);
		glColor4f(1.0, 1.0, 1.0, 1.0);
		agl_textured_rect(bg_textures[button->icon + (state ? 1 : 0)], r.x + BUTTON_PADDING, r.y + BUTTON_PADDING, r.w - 2 * BUTTON_PADDING, r.h - 2 * BUTTON_PADDING, NULL);

		return true;
	}

	void button_set_size (AGlActor* actor)
	{
		actor->region.x2 = actor->region.x1 + 20 + BUTTON_PADDING;
		actor->region.y2 = actor->region.y1 + 20 + BUTTON_PADDING;
	}

	void button_init (AGlActor* actor)
	{
		actor->paint = agl->use_shaders ? gl_button_paint : gl_button_paint_gl1;
	}

	ButtonActor* button = g_new0(ButtonActor, 1);
	*button = (ButtonActor){
		.actor = {
			.init     = button_init,
			.region   = {0, 1, 20 + BUTTON_PADDING, 1 + 20 + BUTTON_PADDING},
			.paint    = agl_actor__null_painter,
			.set_size = button_set_size,
			.on_event = button_on_event,
			.name     = g_strdup_printf("Button %i", icon),
			.disabled = true
		},
		.icon      = icon,
		.action    = action,
		.get_state = get_state,
		.bg_opacity= 0.0,
		.user_data = user_data,
	};

																// they are all same size, why not make animatables an array instead of array of pointers?
	WfAnimatable* bg = AGL_NEW(WfAnimatable,
		.val.f        = &button->bg_opacity,
		.start_val.f  = 0.0,
		.target_val.f = 0.0,
		.type         = WF_FLOAT
	);

	button->animatables = g_malloc(1 * sizeof(WfAnimatable*));
	button->animatables[0] = bg;

	return (AGlActor*)button;
}


static void
button_free (AGlActor* actor)
{
	g_free(((ButtonActor*)actor)->animatables[0]);
	g_free(((ButtonActor*)actor)->animatables);
	g_free(actor->name);
}


static bool
button_on_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	ButtonActor* button = (ButtonActor*)actor;

	g_return_val_if_fail(!actor->disabled, AGL_NOT_HANDLED);

	void animation_done (WfAnimation* animation, gpointer user_data)
	{
	}

	switch (event->type){
		case GDK_ENTER_NOTIFY:
			dbg (1, "ENTER_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_H_DOUBLE_ARROW);

			button->bg_opacity = 1.0;
			if(actor->root->enable_animations){
				agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[0]), animation_done, NULL);
			}else{
				button->bg_opacity = 1.0;
				agl_actor__invalidate(actor);
			}
#ifdef DEBUG_ACTOR
			if(button->tooltip) button->tooltip(actor, actor->name);
#endif
			break;
		case GDK_LEAVE_NOTIFY:
			dbg (1, "LEAVE_NOTIFY");
			//set_cursor(actor->root->widget->window, CURSOR_NORMAL);

			button->bg_opacity = 0.0;
			if(actor->root->enable_animations){
				agl_actor__start_transition(actor, g_list_append(NULL, button->animatables[0]), animation_done, NULL);
			}else{
				button->bg_opacity = 0.0;
				agl_actor__invalidate(actor);
			}
			if(button->tooltip) button->tooltip(actor, NULL);
			break;
		case GDK_BUTTON_RELEASE:
			dbg(1, "BUTTON_RELEASE");
			call(button->action, actor, button->user_data);
			break;
		default:
			break;
	}
	return AGL_HANDLED;
}


void
gl_button_set_sensitive (AGlActor* actor, gboolean sensitive)
{
	dbg(1, "%s: sensitive=%i", actor->name, sensitive);
	actor->disabled = !sensitive;
	agl_actor__invalidate(actor);
}


