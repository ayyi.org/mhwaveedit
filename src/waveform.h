#ifndef __mh_waveform_view_h__
#define __mh_waveform_view_h__

void mh_waveform_view_set_document (WaveformView*, Document*);
void mh_waveform_view_set_vscale   (WaveformView*, float);

#endif //__mh_waveform_view_h__
