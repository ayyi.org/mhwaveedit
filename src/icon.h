
void       icons_init                      ();
GdkPixbuf* get_icon                        (const char*, int size);
GtkWidget* svg_image_new                   (const char* svgpath);

void       create_bitmap_and_mask_from_xpm (GdkBitmap**, GdkBitmap** mask, gchar** xpm);
void       init_cursors                    ();

