/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __wavepool_h__
#define __wavepool_h__

#include "glib.h"

extern GHashTable* wavepool;

Waveform* wavepool_lookup (const char* filename);
Waveform* wavepool_add    (const char* filename);
void      wavepool_remove (Waveform*);

#endif
