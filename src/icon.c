/*
  This file is part of the Ayyi Project. http://www.ayyi.org
  copyright (C) 2004-2014 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#include "config.h"
#include <gtk/gtk.h>
#include "debug.h"
#define RSVG_DISABLE_DEPRECATION_WARNINGS
#include <librsvg/rsvg.h>

#include "icon.h"

static GHashTable* icons = NULL;

static GdkPixbuf* svg_pixbuf_new(const char* filename, int size);


void
icons_init()
{
	icons = g_hash_table_new(g_str_hash, g_str_equal);
}


GdkPixbuf*
get_icon(const char* name, int size)
{
	//name - the svg file name without the svg extension.

	//the icon cache only holds icons with size of 16px.

	//caller must unref the pixbuf when finished with it.

	dbg(2, "name=%s", name);
	GdkPixbuf* pixbuf = (size == 16) ? g_hash_table_lookup(icons, name) : NULL;
	if(pixbuf){
		g_object_ref(pixbuf);
	}else{
		if(size == 16) dbg(3, "icon cache miss. '%s'", name);
		gchar* filename = g_strdup_printf("%s.svg", name);
		if((pixbuf = svg_pixbuf_new(filename, size)) && (size == 16)){
			g_hash_table_insert(icons, (char*)name, pixbuf);
			g_object_ref(pixbuf);
		}
		g_free(filename);
	}
	return pixbuf;
}


#define SVG_DIR "../share/pixmaps/"
GtkWidget*
svg_image_new(const char* svgpath)
{
	//creates a new gtkimage from an svg file.

	gchar* path = g_strdup_printf("%s/%s.svg", SVG_DIR, svgpath);

	if(!g_file_test(path, G_FILE_TEST_EXISTS)) pwarn("file doesnt exist '%s'", path);

	GdkPixbuf* pixbuf = get_icon(svgpath, 16);
	if(!pixbuf) pwarn("svg load failed '%s'.", path);

	//put rendered svn icon onto a widget:
	GtkWidget* image = gtk_image_new_from_pixbuf(pixbuf);

	g_free(path);
	return image;
}


static GdkPixbuf*
svg_pixbuf_new(const char* filename, int size)
{
	char* path = g_strdup_printf("%s/%s", SVG_DIR, filename);

	GdkPixbuf* pixbuf = rsvg_pixbuf_from_file_at_size(path, size, size, NULL);
	if(!pixbuf) pwarn("svg load failed: %s", path);
	g_free(path);
	return pixbuf;
}

//-------------------------------------------------



void
create_bitmap_and_mask_from_xpm (GdkBitmap** bitmap, GdkBitmap** mask, gchar** xpm)
{
  int height, width, colors;
  char pixmap_buffer [(32 * 32)/8];
  char mask_buffer [(32 * 32)/8];
  int x, y, pix;
  int transparent_color, black_color;

  sscanf (xpm [0], "%d %d %d %d", &height, &width, &colors, &pix);

  g_assert (height == 32);
  g_assert (width == 32);
  g_assert (colors == 3);

  transparent_color = ' ';
  black_color = '.';

  for (y = 0; y < 32; y++) {
    for (x = 0; x < 32;) {
      char value = 0, maskv = 0;

      for (pix = 0; pix < 8; pix++, x++) {
	if (xpm [4+y][x] != transparent_color) {
	  maskv |= 1 << pix;

	  if (xpm [4+y][x] != black_color) {
	    value |= 1 << pix;
	  }
	}
      }

      pixmap_buffer [(y * 4 + x/8) - 1] = value;
      mask_buffer [(y * 4 + x/8) - 1] = maskv;
    }
  }

  *bitmap = gdk_bitmap_create_from_data (NULL, pixmap_buffer, 32, 32);
  *mask = gdk_bitmap_create_from_data (NULL, mask_buffer, 32, 32);
}

