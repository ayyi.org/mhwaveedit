Ayyi-mhwaveedit is a modified version of [Mhwaveedit](http://gna.org/projects/mhwaveedit/)
by Magnus Hjorth

Its purpose is to test and showcase the use of [libwaveform](https://github.com/ayyi/libwaveform)
A quick way to install libwaveform from git is to run the script at [test/debian-10/scripts/libwaveform](test/debian-10/scripts/libwaveform)

The original application is unmodified except for replacement
of the main wave display.

Please see README.mhwaveedit for the original details.
