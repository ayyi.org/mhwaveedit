#!/bin/sh

echo 'Generating files...'
libtoolize --automake
aclocal -I m4
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf

